/** import decorators from Mongoose 
 * @Prop() : use to define DB collections properties
 * @Schema() : mark a class to schema definition
 * @SchemaFactory(): decorator will generate the schema
 */ 
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
export type UserDocument = User & Document;
@Schema()
export class User {
    @Prop({required:true})
    fullname: string;
    @Prop({required:true, unique:true, lowercase:true})
    email: string;
    @Prop({required:true})
    password: string
    @Prop({default: Date.now() })
    createdDate: Date
}
export const UserSchema = SchemaFactory.createForClass(User)