import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './controller-user/user.controller';
import { UserService } from './service/user/user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { VideoService } from './service/video/video.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { JwtModule } from '@nestjs/jwt';
import { secret } from './utils/constants';
import { join } from 'path/posix';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://hugues:pU2ZpeXo2PdITt31@cluster4.jr5n23j.mongodb.net/test'),
    JwtModule.register({
      secret,
      signOptions: { expiresIn: '2h' },
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
  ],
  controllers: [AppController, UserController],
  providers: [AppService, UserService, VideoService ],
})
export class AppModule {
  
}
