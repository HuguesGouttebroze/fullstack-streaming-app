# a streaming video app
* Fullstack JavaScript tools

- NestJS, back-end
- ReactJS, front-end
- MongoDB, data-base

## Step to implements on NestJS (back) & React(Front) & MongoDB(noSQL Data-Base)
1.
2.
3. Setting up the Nest server

4. Setting up the MongoDB database

5. Define schemas

6. Defining the application routes
Now that the schema has been defined, it’s time to define the application’s routes. Let’s start by creating a user.controller.ts file in the controllers directory.

Next, we’ll import the decorators needed for the user route, import the User schema class, UserService class (which we’ll create a little later in this article), and the JwtService class to handle user authentication:

```ts
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, UploadedFiles, Put, Req, Res } from "@nestjs/common";
import { User } from "../model/user.schema";
import { UserService } from "../model/user.service";
import { JwtService } from '@nestjs/jwt'

-  use the `@Controller()` decorator to create the Signup and Signin routes, passing the api URL. 
- We’ll also create a `UserController` constructor fnc where we’ll create variables for the `userService` and the `JwtService` class.

```ts
@Controller('/api/v1/user')
export class UserController {
    constructor(private readonly userServerice: UserService,
        private jwtService: JwtService
    ) { }
```

4. STEPS 4
- use Mongoose to connect the application to the MongoDB database.

- set up a MongoDB database for the application. Open the /src/app.module.ts file, and add the following snippet:

TODO: add SECRETS connection URL to a `.env` file and add it to `.gitinore` folder

```ts
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [
     MongooseModule.forRoot('mongodb://localhost:27017/SECRET'),
  ],
```
- import the `MongooseModule` into the root `AppModuleand` use the `forRoot` method to configure the database.

## NestJS, node.js back-end framework

* `Nest.js`, write in `TypeScript`
<p align="center"> 
  <a href="http://nestjs.com/" target="blank">NestJS documentation
  <img src="https://nestjs.com/img/logo-small.svg" width="60" alt="Nest Logo" /></a>
</p>

### Install dependencies

- [Mongoose](https://mongoosejs.com/docs/api.html): 
    Node.js-based ODM library for MongoDB
- Multer: 
    Middleware for handling file uploads
- JSON web token (JWT): 
    Authentication handler
- Universality unique ID (UUID): 
    Random file name generator

To install `Mongoose`, `Multer`, JWT`, and `UUID` packages, run :

```bash
$ npm i -D @types/multer @nestjs/mongoose mongoose @nestjs/jwt passport-jwt @types/bcrypt bcrypt @types/uuid @nestjs/serve-static
```
### Set up Nest server

* create additional folders in the `src` directory :
  - a model, a controller service and utils directories

### Node.js CORS middleware, Express.js
+ [`CORS`-`ExpressJS`](https://github.com/expressjs/cors)
  * Open `src/main.ts` file and enable the [`CORS`](https://www.npmjs.com/package/cors) `ExpressJS` `npm` package by adding the following snippet to the boostrap function:

```ts
 app.enableCors();
```

### Setting up the MongoDB database 

* Use `Mongoose` ODM to connect app to MongoDB.
  - Set up new `MongoDB` database. Into`./src/app.module.ts` file, add the following snippet:

```ts
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [
     MongooseModule.forRoot('mongodb://localhost:27017/Stream'),
  ],
```
 - here, we import the `MongooseModule` into the root `App.Module` use the `forRoot` method to configure the database.

+ Defining the schema

  - Appli has been connected to the `MongoDB` database, define the database schema, required by the appli :
    - into `./src/model` folder, create a `user.schema.ts` file, add:

```ts
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
export type UserDocument = User & Document;
@Schema()
export class User {
    @Prop({required:true})
    fullname: string;
    @Prop({required:true, unique:true, lowercase:true})
    email: string;
    @Prop({required:true})
    password: string
    @Prop({default: Date.now() })
    createdDate: Date
}
export const UserSchema = SchemaFactory.createForClass(User)
```
+ define the video schema that will import mongoose and User schema class. This will enable us to reference and save the details about users who create videos with the app.

### Routes

* Defining the application routes
  - `user` route
  - `UserService` class 
      & 
    `JwtService` classto handle user authentification

```ts
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, UploadedFiles, Put, Req, Res } from "@nestjs/common";
import { User } from "../model/user.schema";
import { UserService } from "../model/user.service";
import { JwtService } from '@nestjs/jwt'
```

## Decorators to define routes

* on User controller, we used Decorators as
    - `@Controller()` decorator to create the `Signup` and `Signin` routes, passing the `api` URL
    - `@Post()` decorator to create `Signup` and `Signin` routes, both of which will listen for a `Post` request
    - `@Res()` decorator to send a reponce to client
    - `@Body()` decorator to parse the data in the request body, in the `Signup` route
    
* We’ll use the `@Controller()` decorator to create the Signup and Signin routes, passing the api URL. We’ll also create a `UserController` class with a constructor function where we’ll create variables for the `userService` class and the `JwtService` class.

```ts
@Controller('/api/v1/user')
export class UserController {
    constructor(private readonly userServerice: UserService,
        private jwtService: JwtService
    ) { }
```

Nest, we use the `@Post` decorator to create the `Signup` and `Signin` routes, both of which will listen for a Post request:

```ts
@Post('/signup')
    async Signup(@Res() response, @Body() user: User) {
        const newUSer = await this.userServerice.signup(user);
        return response.status(HttpStatus.CREATED).json({
            newUSer
        })
    }
    @Post('/signin')
    async SignIn(@Res() response, @Body() user: User) {
        const token = await this.userServerice.signin(user, this.jwtService);
        return response.status(HttpStatus.OK).json(token)
    }
}
```
use the `@Res()` decorator to send a response to the client, and the `@Body()` decorator to parse the data in the request body of the Signup route.

We create a new user by sending the user Schema object to the userService signup method and then return the new user to the client with a 201 status code using the inbuilt Nest HttpsStatus.CREATED method.

We send the user schema object and the jwtService as parameters for the Signin routes. Then, we invoke the Signin method in the userService to authenticate the user and return a token to the client if the sign-in is successful.

## Decorators types Memo on Typescript

* `Typescript` give us the Decorators types, 
  + decorators list we used 
    - @Prop()
    - @Schem()
    - @ShemaFactory()
    
    - @Controller() decorator to create the `Signup` and `Signin` routes, passing the `api` URL
    - @Res() decorator to send a reponce to client
    - @Body() decorator to parse the data in the request body, in the `Signup` route
    - @Post()
-----------------------------
_____________________________
## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

<p align="center"> 
  <a href="http://nestjs.com/" target="blank">NestJS documentation
  <img src="https://nestjs.com/img/logo-small.svg" width="60" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
